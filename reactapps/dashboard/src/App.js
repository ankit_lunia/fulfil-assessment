import React, { Component } from 'react';
import {HashRouter as Router, Link, Route } from 'react-router-dom'
import Home from './components/Home'
import Product from './components/Product'
import Nav from './components/Nav'

class App extends Component {
  render(){
    return (
      <Router>
        <Nav></Nav>
        <div className="container">
          <Route exact path='/' component={Home}/>
          <Route exact path='/products' component={Product}/>
        </div>
      </Router>
    );
  }
}

export default App;
