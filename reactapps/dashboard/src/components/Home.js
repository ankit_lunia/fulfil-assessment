import React, { Component } from 'react'

class Home extends Component{
    constructor(props){
        super(props);

        this.uploadFiles = this.uploadFiles.bind(this);
    }

    uploadFiles(){
        var files = window.$("#file-input")[0].files;
        if (files.length > 0){
            for (var i = 0; i < files.length; i++){
                if(files[i].type != 'text/csv'){
                    alert('Unsupported file type(s). Please upload only csv files.');
                    window.$("#product-upload-form").trigger('reset');
                    return;
                }
            }
        } else{
            alert('Please select few files to upload!');
            return;
        }
        var formData = new FormData(window.$("#product-upload-form")[0]);
        window.$("#upload-button").attr('disabled', 'disabled');

        window.$.ajax({
            url: '/upload-products',
            type: 'post',
            data: formData,
            contentType: false,
            enctype: 'multipart/form-data',
            processData: false,
            success:function(resp){
                window.$("#product-upload-form").trigger('reset');
                window.$("#upload-button").removeAttr('disabled', 'disabled');
                alert(resp.message);
            },
            error: function(xhr, status, error){
                window.$("#upload-button").removeAttr('disabled', 'disabled');
                alert(error);
            }

        });
        
    }

    render(){
        return (
            <div className="container">
                <div className="card">
                    <div className="card-content white-text center">
                        <form id="product-upload-form">
                            <div className="file-field input-field">
                                <div className="btn">
                                    <span>Add files (.csv)</span>
                                    <input id="file-input" name="files" type="file" multiple />
                                </div>
                                <div className="file-path-wrapper">
                                    <input className="file-path validate" type="text" placeholder="Upload one or more files" />
                                </div>
                            </div>
                        </form>
                        <a id="upload-button" className="waves-effect waves-light btn-small" onClick={this.uploadFiles}>Upload</a>
                    </div>
                </div>
            </div>
        )
    }
}

export default Home;
