import React, { Component } from 'react'
import {withRouter} from 'react-router-dom';

class Product extends Component{
    constructor(props){
        super(props);

        this.state = {products:[], total_pages:0, current_page:1, show_number_of_pages:7, last_page:1};

        this.fetchProducts = this.fetchProducts.bind(this);
        this.filterProducts = this.filterProducts.bind(this);
        this.deleteAllProducts = this.deleteAllProducts.bind(this);
    }

    deleteAllProducts(){
        var me = this;
        var ans = window.confirm('Are you sure?');
        if (!ans){
            return;
        }
        window.$("#delete-button").attr('disabled', 'disabled');
        window.$.ajax({
            url: '/products',
            type: 'delete',
            success:function(resp){
                alert(resp.message);
                window.$("#delete-button").removeAttr('disabled', 'disabled');
                me.setState({products:[], total_pages:0, current_page:1, last_page:1});
                setTimeout(function(){
                    me.fetchProducts(me.state.current_page, '', '', 0);
                })
            },
            error: function(xhr, status, error){
                window.$("#delete-button").removeAttr('disabled', 'disabled');
                alert(error);
            }
        })
    }

    filterProducts(){
        var page = 1;
        var sku = window.$("#product_sku").val();
        var name = window.$("#product_name").val();
        var active = window.$("#product_active").prop('checked') ? 1 : 0;
        this.fetchProducts(page, sku, name, active);
    }

    componentDidMount(){
        this.fetchProducts(this.state.current_page, '', '', 0);
    }

    fetchProducts(page, sku, name, active){
        var me = this;
        if (page<1 || page>this.state.last_page){
            return;
        }
        window.$.ajax({
            url: '/products',
            type: 'get',
            data: {page: page, sku: sku, name: name, active: active},
            success:function(resp){
                me.setState({products: resp.products, total_pages: resp.total_pages, last_page: resp.total_pages ? resp.total_pages : 1, current_page: resp.current_page});
            },
            error: function(xhr, status, error){
                alert(error);
            }
        })
    }
    render(){
        var products = this.state.products.map(function(product){
            return  <tr key={product.id}> 
                        <td>{product.sku}</td>
                        <td>{product.name}</td>
                        <td>{product.description}</td>
                        <td>{product.active?'Yes':'No'}</td>
                    </tr>;
        });
        var pagination = [];
        var start = this.state.current_page - 3;
        var end = this.state.current_page + 3;
        var cursor_style = {cursor:'pointer'};
        if (start < 1){
            start = 1;
            end = 7;
        }
        pagination.push(
            <li key="prev" className={this.state.current_page == 1 ? 'disabled' : ''} onClick={(e) => {this.fetchProducts(this.state.current_page - 1, '', '', 0)}}><a style={this.state.current_page == 1 ? {} : cursor_style}><i className="material-icons">chevron_left</i></a></li>
        );
        for (var page=start; page <= this.state.total_pages && page <= end; page++){
            pagination.push(
                <li key={page} eventkey={page} className={this.state.current_page == page ? "active" : "wave-effect"} onClick={(e) => {this.fetchProducts(parseInt(e.target.getAttribute('eventkey')), '', '', 0)}}><a style={this.state.current_page == page ? {} : cursor_style} eventkey={page}>{page}</a></li>
            );
        }
        pagination.push(
            <li key="next" className={this.state.current_page == this.state.total_pages ? 'disabled' : ''} onClick={(e) => {this.fetchProducts(this.state.current_page + 1, '', '', 0)}}><a style={this.state.current_page == this.state.total_pages ? {} : cursor_style}><i className="material-icons">chevron_right</i></a></li>
        );
        return (
            <div className="card row">
                <div className="">
                    <form className="col s9">
                        <div className="row">
                            <div className="input-field col">
                                <input id="product_sku" type="text" className="validate" />
                                <label htmlFor="product_sku">Sku</label>
                            </div>
                            <div className="input-field col">
                                <input id="product_name" type="text" className="validate" />
                                <label htmlFor="product_name">Name</label>
                            </div>
                            <div className="input-field col">
                                <p>
                                    <label>
                                        <input id="product_active" type="checkbox" />
                                        <span>Active</span>
                                    </label>
                                </p>
                            </div>
                            <div className="input-field col">
                                <a className="btn-floating btn-medium waves-effect waves-light" onClick={this.filterProducts}><i className="material-icons">search</i></a>
                            </div>
                        </div>
                    </form>
                    <div className="col s3" style={{marginTop:'20px'}}>
                    <a id="delete-button" className="btn btn-small waves-effect waves-light" onClick={this.deleteAllProducts}>Delete all records</a>
                    </div>
                    <div className="col s12">
                        {
                            this.state.total_pages ?
                            <ul className="pagination">
                                {pagination}
                            </ul>: null

                        }
                    </div>
                </div>
                <div className="col s12">
                    <table className="responsive-table">
                        <thead>
                            <tr>
                                <th>Sku</th>
                                <th>Name</th>
                                <th>Description</th>
                                <th>Active</th>
                            </tr>
                        </thead>

                        <tbody>
                            {products}
                        </tbody>
                    </table>
                </div>
            </div>
        )
    }
}

export default withRouter(Product);
