import React, { Component } from 'react'
import {HashRouter as Router, Link, Route } from 'react-router-dom'
import {withRouter} from 'react-router-dom';

class Nav extends Component{
    render(){
        return (
            <nav>
                <div className="nav-wrapper">
                <a className="brand-logo">Dashboard</a>
                <ul id="nav-mobile" className="right hide-on-med-and-down">
                    <li className={this.props.location.pathname == '/' ? "active" : ''}><Link to="/">Home</Link></li>
                    <li className={this.props.location.pathname == '/products' ? "active" : ''}><Link to="/products">View Products</Link></li>
                </ul>
                </div>
            </nav>
        )
    }
}

export default withRouter(Nav);
