from application.config.base import Config

class ProductionConfig(Config):
    DEBUG = False
    SQLALCHEMY_DATABASE_URI = 'postgres://vvbaowzpgmuqvp:04d0ee75017773b117e97c028bad3d206b7435d5acd9d890f8f87df6af7a4788@ec2-23-21-186-85.compute-1.amazonaws.com:5432/d5dhbq3hq339j0'
    CELERY_RESULT_BACKEND='amqp://cvsrjrlr:keRp91nk1TBsG6qev-RMDP0IYH6jZkEm@orangutan.rmq.cloudamqp.com/cvsrjrlr'
    CELERY_BROKER_URL='amqp://cvsrjrlr:keRp91nk1TBsG6qev-RMDP0IYH6jZkEm@orangutan.rmq.cloudamqp.com/cvsrjrlr'
    CELERY_TASK_DEFAULT_EXCHANGE = 'cvsrjrlr'
    TMP_DIR = "tmp"