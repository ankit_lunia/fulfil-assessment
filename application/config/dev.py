from application.config.base import Config

class DevelopmentConfig(Config):
    DEVELOPMENT = True
    DEBUG = True