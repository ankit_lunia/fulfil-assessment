class Config(object):
    DEBUG = False
    TESTING = False
    CSRF_ENABLED = True
    SECRET_KEY = 'some secret key'
    SQLALCHEMY_DATABASE_URI = 'postgresql://localhost/fulfil'
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    CELERY_RESULT_BACKEND = 'amqp://fulfil:fulfil@localhost:5672/fulfil'
    CELERY_BROKER_URL = 'amqp://fulfil:fulfil@localhost:5672/fulfil'
    CELERY_TASK_DEFAULT_EXCHANGE = 'fulfil'
    CELERY_ACKS_LATE = False
    CELERY_BEARER_TOKEN = "some token"
    MAX_CONTENT_LENGTH = 100 * 1024 * 1024
    TMP_DIR = "/tmp"
    DROPBOX_API_TOKEN = '53vzvO5fmTAAAAAAAAAJ0g_cuCW6Qk6hGvD4F0ukS1-mCUt684FcLoI2gxOKV6Xd'
