import os
import sys
sys.path.append(os.getcwd())

from flask import Flask
from application.utils.common import create_app
from application.blueprints.product.views import blueprint as product_blueprint

app = create_app(__name__)
app.app_context().push()

app.register_blueprint(product_blueprint, url_prefix='/')

if __name__ == '__main__':
    app.run()