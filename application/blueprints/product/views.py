from flask import jsonify, Blueprint, request, render_template, current_app
from application.tasks.products import process_products
from application.models.product import Product
from werkzeug import secure_filename
import os
from celery import group
from application.models import db
from application.utils.dropbox_client import dbx
import dropbox

blueprint = Blueprint('product_api', __name__)

@blueprint.route('/', methods=["GET"])
def index():
    return render_template("index.html")

@blueprint.route('/products', methods=["GET"])
def get_products():
    try:
        sku = request.args.get('sku')
        name = request.args.get('name')
        try:
            active = int(request.args.get('active'))
            active = True if active else False
        except:
            active = False
        try:
            page = int(request.args.get('page'))
            page = page if page else 1
        except:
            page = 1

        products_query = Product.query.filter_by(active=active)
        if sku:
            products_query = products_query.filter(Product.sku.like("%"+sku+"%"))
        if name:
            products_query = products_query.filter(Product.name.like("%"+name+"%"))

        products = products_query.paginate(page=page, per_page=50)
        total_pages = products.pages
        products = [product.get_details() for product in products.items]
        return jsonify({'success': True, 'products': products, 'total_pages': total_pages, 'current_page': page})
    except Exception as e:
        print(e)
        return jsonify({'success': False, 'error': e.message}), 500

@blueprint.route('/upload-products', methods=["POST"])
def add_products():
    try:
        files = request.files.getlist("files")
        if not files:
            raise ("Please select some files")
        for file in files:
            if file.content_type != 'text/csv':
                raise ('Please upload csv files only!')
            
            filename = secure_filename(file.filename)
            meta = dbx.files_upload(file.stream.read(), "/"+filename, mode=dropbox.files.WriteMode("overwrite"))
            
            process_products.delay(filename)
        
        return jsonify({'success': True, 'message': 'Successfully uploaded file(s). Sit back till we process the file.'})
    except Exception as e:
        print (e)
        return jsonify({'success': False, 'error': e.message}), 500


@blueprint.route('/products', methods=["DELETE"])
def delete_products():
    try:
        Product.query.delete()
        db.session.commit()
        return jsonify({'success': True, 'message': 'Successfully deleted all records.'})
    except Exception as e:
        print(e)
        return jsonify({'success': False, 'error': e}), 500