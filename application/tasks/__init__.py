import os
import sys
sys.path.append(os.getcwd())

from flask import Flask
from celery import Celery
from application.models import db

def create_celery_app(__name__):
    app = Flask(__name__)

    if os.environ.get("ENVIRONMENT", "dev").lower() == "production":
        app.config.from_object('application.config.prod.ProductionConfig')
    else:
        app.config.from_object('application.config.dev.DevelopmentConfig')
    db.init_app(app)

    return app

app = create_celery_app(__name__)
app.app_context().push()
celery_app = Celery(app.name, broker=app.config['CELERY_BROKER_URL'], include=['application.tasks.products'])