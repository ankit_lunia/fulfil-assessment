import os
import sys
sys.path.append(os.getcwd())

from application.tasks import celery_app
from application.models import db
from application.models.product import Product
import csv
from application.utils.dropbox_client import dbx

@celery_app.task(bind=True, default_retry_delay=10, max_retries=1, queue='products')
def process_products(self, filepath):
    products = []
    chunk = 0
    try:
        _, res = dbx.files_download('/'+filepath)
        csvreader=csv.reader(res.raw)
        fields = next(csvreader)
        
        for row in csvreader:
            product = Product.query.filter_by(sku=row[1].strip()).first()
            if not product:
                product = Product()
            product.name = row[0].strip()
            product.sku = row[1].strip()
            product.description = row[2].strip()
            db.session.add(product)
            db.session.flush()
            chunk += 1
            if chunk % 1000:
                db.session.commit()
        
        db.session.commit()
        return {'success': True}
    except Exception as e:
        print(e)
        raise    