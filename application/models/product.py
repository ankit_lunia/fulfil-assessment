from sqlalchemy import String, Integer, Column, DateTime, text, Boolean
from application.models import db

class Product(db.Model):
    __tablename__ = 'products'
    created_at = Column(DateTime, server_default=db.func.now())
    updated_at = Column(DateTime, server_default=db.func.now(), server_onupdate=db.func.now())

    id = Column(Integer, primary_key=True, autoincrement=True)
    name = Column(String)
    description = Column(String)
    sku = Column(String, unique=True)
    active = Column(Boolean, default=False)

    def get_details(self):
        return {
            'active': self.active,
            'created_at': self.created_at,
            'description': self.description,
            'id': self.id,
            'name': self.name,
            'sku': self.sku,
            'updated_at': self.updated_at
        }