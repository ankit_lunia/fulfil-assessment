import os
import sys
sys.path.append(os.getcwd())

from flask_script import Manager
from flask_migrate import Migrate, MigrateCommand

from application.app import app
from application.models import db
from application.models.product import Product

migrate = Migrate(app, db)
manager = Manager(app)

manager.add_command('db', MigrateCommand)


if __name__ == '__main__':
    manager.run()