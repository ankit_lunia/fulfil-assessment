from flask import current_app
import dropbox

dbx = dropbox.Dropbox(current_app.config.get('DROPBOX_API_TOKEN'))