from flask import Flask
from application.models import db
import os

def create_app(__name__):
    app = Flask(__name__)

    if os.environ.get("ENVIRONMENT", "dev").lower() == "production":
        app.config.from_object('application.config.prod.ProductionConfig')
    else:
        app.config.from_object('application.config.dev.DevelopmentConfig')
    db.init_app(app)

    return app